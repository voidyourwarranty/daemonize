/*
// ================================================================================
// GitLab:    http://gitlab.com/voidyourwarranty/daemonize
// File:      daemonize.c
// Date:      2018-10-16
// Author:    Void Your Warranty
// License:   Gnu General Public License GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
// Purpose:   Turn a Process into a Daemon.
// ================================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <pwd.h>
#include "daemonize.h"

#define EXIT_GOOD 0
#define EXIT_BAD -1

/*
// Read the PID from the lock file whose full path is given by <lock>. Here <name> is the name of the daemon as it is
// used in error messages.
//
// If <lock> is NULL or an empty string, the function returns 0.
// If there does not exist any regular file <lock>, the function returns 0.
// If there exists a regular file, the function returns the PID that it has read from that file.
// In case of an error, a message is written to <stderr>, and the function returns -1.
*/

static int read_pid ( const char *name, const char *lock ) {

  struct stat sb;
  char        buff[256];
  pid_t       pid;
  int         fd,n;

  if ((!lock) || (!strlen (lock)))
    return (0);

  /* Check whether there exists a regular file <lock>. */
  if ((stat (lock,&sb)) || (!S_ISREG (sb.st_mode)))
    return (0);

  fd = open (lock,O_RDONLY);
  if (fd < 0) {
    fprintf (stderr,"%s: unable to open PID file '%s': %s (%d).\n",name,lock,strerror (errno),errno);
    return (-1);
  }

  n = read (fd,buff,255);
  if (n <= 0) {
    fprintf (stderr,"%s: unable to read PID file '%s': %s (%d).\n",name,lock,strerror (errno),errno);
    return (-1);
  }

  buff[n] = 0;
  pid = atoi (buff);

  if (pid <= 0) {
    fprintf (stderr,"%s: internal error: corrupt PID file '%s' with contents '%s'.\n",name,lock,buff);
    return (-1);
  }

  return (pid);
}

/*
// The parent process that launches the daemon, receives feedback from its child, the daemonized process. Upon success,
// the parent receives a SIGUSR1. If there is an error, the child process is terminated, and after some timeout when no
// SIGUSR1 arrives, the parent receives a SIGALRM.
//
// The following signal handler deals with this feedback. The handler always belongs to the parent. It receives two
// strings in static global variables.
//
// Note that these static global variables are duplicated when the process forks, and so we have to make sure they are
// set either before the process forks or they are set by the parent process itself.
*/

static const char *daemonname = NULL; /* name of the daemon for error messages */
static const char *lockfile = NULL;   /* full path of the lock file */

static void signal_from_child ( int signum ) {

  pid_t pid;
  
  if (!daemonname) {
    fprintf (stderr,"internal error: no daemon name.\n");
    exit (EXIT_BAD);
  }
  
  switch(signum) {

  case SIGALRM:

    fprintf (stderr,"%s: internal error: child process timed out.\n",daemonname);
    exit (EXIT_BAD);
    break;

  case SIGUSR1:

    pid = read_pid (daemonname,lockfile);
    if (pid < 0) {
      fprintf (stderr,"%s: internal error: cannot read lock file '%s'.\n",daemonname,lockfile);
      exit (EXIT_BAD);
    }

    if (pid > 0) {
      fprintf (stdout,"%s: daemon successfully launched with PID %d.\n",daemonname,pid);
      exit (EXIT_GOOD);
    }

    if ((lockfile) && (strlen (lockfile) > 0)) {
      fprintf (stderr,"%s: daemon successfully launched, but the lock file '%s' is missing.\n",daemonname,lockfile);
      exit (EXIT_GOOD);
    }

    fprintf (stderr,"%s: daemon successfully launched.\n",daemonname);
    exit (EXIT_GOOD);
    break;
  }
}

/*
// Once the daemonized process runs, it needs to handle at least the signals SIGHUP (reload the configuration files and
// restart) and SIGTERM (initiate a clean shutdown). The following signal handler passes these conditions on to the
// daemon in static global variables for which we then provide acess functions. This handler always belongs to the
// child, i.e. to the daemonized process.
*/

static int isterm = 0; /* have received a SIGTERM or SIGINT ? */
static int ishup = 0;  /* have received a SIGHUP ? */

static void signal_to_child ( int signum ) {

  switch (signum) {
  case SIGINT:
    syslog (LOG_INFO,"Received SIGINT.");
    isterm = 1;
    break;
    
  case SIGTERM:
    syslog (LOG_INFO,"Received SIGTERM.");
    isterm = 1;
    break;
    
  case SIGHUP:
    syslog (LOG_INFO,"Received SIGHUP.");
    ishup = 1;
    break;
  }
}

/*
// Access functions in order to read out the static global variables.
*/

int is_term ( void ) {
  int i = isterm;
  isterm = 0;
  return (i);
}

int is_hup ( void ) {
  int i = ishup;
  ishup = 0;
  return (i);
}

/*
// Turn the current process into a daemon. This function performs the required forking of processes. The parent process
// returns to the caller with a suitable exit code EXIT_GOOD or EXIT_BAD. It writes a brief status message to <stdout>
// or an error message to <stderr>, respectively.
//
// The following function returns only for a child process that has successfully been converted into a daemon.
//
// <name> - the name of the daemon for use in error messages
// <lock> - the full path of the lock file or NULL if lock files are not used
// <log>  - the full path of a log file or NULL if <stdout> and <stderr> of the daemon are to be discarded
// <cdir> - the current directory to run the daemon in (in order not to lock a random other directory)
// <user> - the user with whose rights to execute the daemon if its was run as root or NULL if no switch of users is desired
// <wait> - the timeout in seconds after which the parent reports an error during creation of the daemon
//
// Note that only the parent process outputs to <stdout> and <stderr>. The child process that is turned into the daemon
// reports important steps to the syslog facility. The daemon program code has the choice of using syslog, too, or
// writing to <stderr> or <stdout> and provide a <log> file to redirect these to.
*/

void daemon_launch ( const char *name, const char *lock, const char *log, const char *cdir, const char *user, int wait ) {
  
  char        buff[256];
  pid_t       pid,ppid;
  int         fd;

  /* The signal handler of the parent needs these two strings. */
  daemonname = name;
  lockfile = lock;

  /* Check whether there is a lock file */
  pid = read_pid (name,lock);
  if (pid < 0) {
    exit (EXIT_BAD);
  } else if (pid > 0) {

    /* If there is already a process running with that PID, we assume it is this very daemon, and there is nothing to do. */
    if ((kill (pid,0) == 0) || (errno != ESRCH)) {
      fprintf (stderr,"%s: daemon already running with PID %d.\n",name,pid);
      exit (EXIT_GOOD);
    }

    /* If no such process is running, the lock file is a stale one and needs to be removed. */
    unlink (lock);
    fprintf (stdout,"%s: removing stale PID file.\n",name);
  }

  /* If run as root, we switch user if desired. */
  if ((user) && (strlen (user) > 0))
    if ((getuid () == 0) || (geteuid () == 0)) {
      
      struct passwd *pw = getpwnam (user);
      if (pw) {
	if (setuid (pw->pw_uid)) {
	  fprintf (stderr,"%s: unable to switch to user '%s': %s (%d).\n",name,user,strerror (errno),errno);
	  exit (EXIT_BAD);
	} else {
	  fprintf (stdout,"%s: switching to user '%s'.\n",name,user);
	}
      } else {
	fprintf (stderr,"%s: unable to find user '%s'.\n",name,user);
	exit (EXIT_BAD);
      }
    }
  
  fd = -1;
  /* If lock files are used, open the lock file for later writing and keep the file descriptor. */
  if ((lockfile) && (strlen (lockfile) > 0)) {
  
    fd = open (lock,O_RDWR | O_CREAT,0640);
    if (fd < 0) {
      fprintf (stderr,"%s: unable to create lock file '%s': %s (%d).\n",name,lock,strerror (errno),errno);
      exit (EXIT_BAD);
    }
  }

  /* The daemon process that becomes the child tells the parent about its status via this signal handler. */
  signal (SIGUSR1,signal_from_child); /* ... if the child signals success */
  signal (SIGALRM,signal_from_child); /* ... if the child does not come back after <wait> seconds */
  
  /* Fork off and allow the parent to return. */
  pid = fork ();
  if (pid < 0) {
    fprintf (stderr,"%s: unable to fork off the parent: %s (%d).\n",name,strerror (errno),errno);
    exit (EXIT_BAD);
  }

  if (pid > 0) {  /* This is the parent. */
    alarm (wait); /* Wait for a status from the child. If during this time, no SIGUSR1 is received, the parent reports failure. */
    pause ();     /* This will never return. */       
    exit (EXIT_BAD);
  }
  
  /* This is the child. */

  /* Find the parent process. */
  ppid = getppid ();

  /* Move the child to a new session. */
  if (setsid () < 0) {
    fprintf (stderr,"%s: unable to create new session: %s (%d).\n",name,strerror (errno),errno);
    exit (EXIT_BAD);
  }

  /* Fork once more and let the parent return immediately. */
  pid = fork ();
  if (pid < 0) {
    fprintf (stderr,"%s: unable to fork off the second parent: %s (%d).\n",name,strerror (errno),errno);
    exit (EXIT_BAD);
  }
  
  if (pid > 0) { /* This is the parent. */
    exit (EXIT_GOOD);
  }
  
  /* This is the child. */

  /* Write our own pid to the lock file if there is one. */
  if (fd >= 0) {
    sprintf (buff,"%d",getpid ());
    if (write (fd,buff,strlen (buff)) < strlen (buff)) {
      fprintf (stderr,"%s: unable to write lock file '%s': %s (%d).\n",name,lock,strerror (errno),errno);
      exit (EXIT_BAD);
    }
    
    close (fd);
  }
  
  /* Change a number of signal handlers. */
  signal (SIGTSTP,SIG_IGN);         /* Ignore signals from TTY. */
  signal (SIGTTOU,SIG_IGN);
  signal (SIGTTIN,SIG_IGN);
  signal (SIGHUP ,signal_to_child); /* Explicitly handle the following signals. */
  signal (SIGTERM,signal_to_child);
  signal (SIGINT ,signal_to_child);
  
  /* Reset the file mode mask in case we later try to create any files. */
  umask(0);

  /* Reset the current directory in order not to lock any random directory. */
  if ((cdir) && (strlen (cdir) > 0)) {
    if (chdir (cdir) < 0) {
      fprintf (stderr,"%s: unable to change directory to '%s': %s (%d).\n",name,cdir,strerror (errno),errno);
      exit (EXIT_BAD);
    }
  }
  
  /* Close all open file descriptors in order not to block anything. */
  for (int fd = sysconf (_SC_OPEN_MAX); fd >= 0; fd--)
    close (fd);

  /* Begin logging to syslog. */
  openlog (name,LOG_PID,LOG_LOCAL5);
  syslog (LOG_INFO,"Starting.");
  
  /*
  // Redirect the standard file descriptors to /dev/null in case the daemon code accidentally writes something or tries
  // to read something.
  //
  // Alternatively, if a <log> file path is provided, we keep <stdin> at /dev/null, but redirect both <stdout> and
  // <stderr> to the <log> file.
  */
  open ("/dev/null",O_RDWR); /* stdin */
  if (log && (strlen (log) > 0)) {
    if (open (log,O_WRONLY | O_APPEND | O_CREAT,0640) < 0) {  /* stdout */
      syslog (LOG_ERR,"Unable to create log file '%s'.",log);
      open ("/dev/null",O_RDWR);
    }

    dup (1); /* stderr */
  } else {
    dup (0); /* stdout */                  
    dup (0); /* stderr */
  }

  /* Tell the parent that we have successfully launched the daemon. */
  kill (ppid,SIGUSR1);

  /* The function returns to the caller only for the successfully daemonized child. */
}

/*
// Terminate the daemon.
*/

void daemon_shutdown ( const char *lock ) {

  /* Stop using the syslog. */
  syslog (LOG_INFO,"Stopping.");
  closelog ();

  /* Remove any possible lock file. */
  if (lock && (strlen (lock) > 0)) 
    (void)unlink (lock);
}

/*
// ================================================================================
// End of file.
// ================================================================================
*/
