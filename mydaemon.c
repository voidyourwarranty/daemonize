/*
// ================================================================================
// GitLab:    http://gitlab.com/voidyourwarranty/daemonize
// File:      mydaemon.c
// Date:      2018-10-16
// Author:    Void Your Warranty
// License:   Gnu General Public License GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
// Purpose:   Example Deamon.
// ================================================================================
*/

#define DAEMON_NAME "mydaemon"              /* name of the daemon */
#define DAEMON_LOCK "/var/run/mydaemon.pid" /* its lock file */
#define DAEMON_WAIT 2                       /* timeout for launch (seconds) */
#define DAEMON_LOG  "/var/log/mydaemon.log" /* its log file */
#define DAEMON_CDIR "/tmp"                  /* its current directory */
#define DAEMON_USER NULL                    /* user */

#include <stdio.h>
#include <syslog.h>
#include <unistd.h>
#include "daemonize.h"

int main ( void ) {

  int i;

  daemon_launch (DAEMON_NAME,DAEMON_LOCK,DAEMON_LOG,DAEMON_CDIR,DAEMON_USER,DAEMON_WAIT);

  while (1) {
    i++;
    syslog (LOG_INFO,"Daemon counts %d.",i);
    fprintf (stderr,"stderr count %d.\n",i);
    
    sleep (5);

    if (is_hup ()) {
      syslog (LOG_INFO,"I am supposed to restart and re-read all configuration files.");
    }
    
    if (is_term ())
      break;
  }
  
  daemon_shutdown (DAEMON_LOCK);

  return (0);
}

/*
// ================================================================================
// End of file.
// ================================================================================
*/
