# daemonize: How to turn any process into a daemon (Linux)

This project is the result of trying to understand how to launch a daemon on Linux, i.e. a process that detaches from
the console and keeps running in the background even after the caller has logged out.

There are plenty of sources on the internet, for example [Stack Overflow: Linux
daemonize](https://stackoverflow.com/questions/3095566/linux-daemonize), [Stack Overflow: Creating a Daemon in
Linux](https://stackoverflow.com/questions/17954432/creating-a-daemon-in-linux), [codingfreak: Daemon-izing a Process in
Linux](https://codingfreak.blogspot.com/2012/03/daemon-izing-process-in-linux.html), [CK.D.Lee: How to Daemonize in
Linux](https://jiafei427.wordpress.com/2016/11/14/how-to-daemonize-in-linux/), or [Shichao's Notes: Daemon
Processes](https://notes.shichao.io/apue/ch13/).

None of these did exactly what I wanted, and so here is my own version of how to turn a process into a daemon. In the
project directory, call `make` and then the executable `mydaemon` as root, i.e. on Ubuntu this reads

```
sudo ./mydaemon
```

This launches a sample daemon in the background which runs as root and which merely counts forever and writes the
numbers into a log file `/var/log/mydaemon.log` as well as to the syslog. Take a look at
[mydaemon.c](https://gitlab.com/voidyourwarranty/daemonize/blob/master/mydaemon.c) for the usage of the main two functions

```c
void daemon_launch   ( const char *name, const char *lock, const char *log, const char *cdir, const char *user, int wait );
void daemon_shutdown ( const char *lock );
```
Their implementation can be seen in [daemonize.c](http://www.gitlab.com/voidyourwarranty/daemonize/blob/master/daemonize.c).

The daemons started this way
- can run as any user (when they are initially started by root),
- can either suppress the daemon's `stdout` and `stderr` or log them to an arbitrary log file (here: `/var/log/mydaemon.log`),
- can use a lock file (here: `/var/run/mydaemon.pid`) in order to make sure there is at most one daemon process running at any time,
- catch the signals `SIGTERM` or `SIGINT` which stop the daemon as well as `SIGHUP` which is supposed to restart it.

The daemon control functions are always logged via syslog. Only launch feedback goes to `stdout` and `stderr`. The
daemon itself can log to both syslog or a dedicated log file. You can watch the activity of the sample daemon by

```
sudo tail -f /var/log/syslog
sudo tail -f /var/log/mydaemon.log
```

You can send it `SIGTERM` to shutdown or `SIGHUP` in order to restart by (assuming a bash)

```
sudo kill -TERM `sudo cat /var/run/mydaemon.pid`
sudo kill -HUP `sudo cat /var/run/mydaemon.pid`
```

## License

[GNU General Public License GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
