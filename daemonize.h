/*
// ================================================================================
// GitLab:    http://gitlab.com/voidyourwarranty/daemonize
// File:      daemonize.h
// Date:      2018-10-16
// Author:    Void Your Warranty
// License:   Gnu General Public License GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
// Purpose:   Header File.
// ================================================================================
*/

#ifndef _DAEMONIZE_H
#define _DAEMONIZE_H

int  is_term         ( void );
int  is_hup          ( void );
void daemon_launch   ( const char *name, const char *lock, const char *log, const char *cdir, const char *user, int wait );
void daemon_shutdown ( const char *lock );

#endif

/*
// ================================================================================
// End of file.
// ================================================================================
*/
