## ================================================================================
## GitLab:    http://gitlab.com/voidyourwarranty/daemonize
## File:      Makefile
## Date:      2018-10-16
## Author:    Void Your Warranty
## License:   Gnu General Public License GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
## Purpose:   Project Makefile.
## ================================================================================

MAKEFLAGS += --no-builtin-rules

## --------------------------------------------------------------------------------
## Project directories
## --------------------------------------------------------------------------------

BINDIR   = .
OBJDIR   = .
SRCDIR   = .
DEPDIR   = .

## --------------------------------------------------------------------------------
## Executables
## --------------------------------------------------------------------------------

RM = rm -f
CC = gcc -c
LD = gcc

CLEAN = $(programs:%=$(BINDIR)/%) $(OBJDIR)/*.o $(DEPDIR)/*.d 

## --------------------------------------------------------------------------------
## Compiler and linker options
## --------------------------------------------------------------------------------

DEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.d
CPPFLAGS =
LDFLAGS  =
LDLIBS   =

ifeq ($(debug),no)
  CFLAGS = -Wall -O2 -DNDEBUG -march=native
else
  CFLAGS = -Wall -gstabs+
endif

## --------------------------------------------------------------------------------
## Project files
## --------------------------------------------------------------------------------

programs         = mydaemon
mydaemon_modules = daemonize

## --------------------------------------------------------------------------------
## Specific rules for all executables
## --------------------------------------------------------------------------------

all: $(programs:%=$(BINDIR)/%)

$(BINDIR)/mydaemon: $(mydaemon_modules:%=$(OBJDIR)/%.o)

## --------------------------------------------------------------------------------
## General implicit rules
## --------------------------------------------------------------------------------

$(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(CC) $(DEPFLAGS) $(CPPFLAGS) $(CFLAGS) -o $@ $(SRCDIR)/$*.c

$(BINDIR)/% : $(OBJDIR)/%.o
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS)

$(DEPDIR)/%.d: ;

.PRECIOUS: $(DEPDIR)/%.d

.PHONY: doc

clean:
	$(RM) $(CLEAN)

include $(DEPDIR)/*.d

## ================================================================================
## End of file.
## ================================================================================
